var Hapi = require('hapi'),
    clients = require('./clients');

var server = new Hapi.Server();
var port = Number(process.env.PORT ||process.argv[2] || config.port || 8000);

server.connection({ port: port });

server.route([

	{
		method: '*',
        path: '/{p*}',
        handler: function (request, reply) {
            log(request);

            reply("Path or method not supported.").code(400);
        }
	},

	{
		method: 'POST',
        path: '/clients/add',
        handler: function (request, reply) {
            log(request);

            try {
                var client = JSON.parse(request.payload);
                reply((clients.addClient(client)));
            } catch (e) {
                reply("Client object is not formed correctly.").code(400);
            }
        }
	},

    {
        method: 'GET',
        path: '/clients',
        handler: function (request, reply) {
            log(request);

            reply(clients.getAllClients());
        }
    }

]);

server.start(function () {
    console.log('Server running at: ', server.info.uri);
});

function log(request) {
    console.log(new Date().toString() + ": Got a request from " + request.info.remoteAddress +
    " - requesting path: " + request.path);
}