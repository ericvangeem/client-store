
var clients = exports;

var collection = [
    {"id":1,"name":"Imagitas, Inc.","clientSince":"4-23-2011","industry":"Business Services","aemVersion":"6.0"},{"id":2,"name":"Amway","clientSince":"6-15-2013","industry":"Retail / Pyramid Scheme","aemVersion":"5.6.1"},{"id":3,"name":"Lowes","clientSince":"8-12-2012","industry":"Retail / Home Improvement","aemVersion":"6.0"},{"id":4,"name":"NASCAR","clientSince":"3-22-2013","industry":"Entertainment / Racing","aemVersion":"5.5"},{"id":5,"name":"Food Lion","clientSince":"7-4-2014","industry":"Retail / Groceries","aemVersion":"6.0"}
];

var idCount = collection.length;

clients.addClient = function(client) {
    validate(client);

    client.id = idCount + 1;
    collection.push(client);
    idCount++;

    return client;
};


clients.getAllClients = function() {
    return {
        "clients" : collection
    };
};

function validate(client) {
    if (client["name"] == undefined ||
        client["clientSince"] == undefined ||
        client["industry"] == undefined ||
        client["aemVersion"] == undefined) {

        throw "Invalid client object.";
    }
}